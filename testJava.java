import java.util.Scanner;
import java.util.Arrays;

public class testJava {
    public static void main(String[] args) {
        Scanner Keyboard = new Scanner(System.in);
        System.out.print("Enter number Of Card (2-1000) : ");
        int numberCard = Keyboard.nextInt();

        int[] number = new int[numberCard];

        if (numberCard >= 2 && numberCard <= 1000) {
            for (int i = 0; i < numberCard; i++) {
                int inputNumber = Keyboard.nextInt();
                number[i] = inputNumber;
            }
        } else {
            System.out.println("Enter Number 2 - 1000");
        }
        Arrays.sort(number);

        for (int i = 0; i < numberCard; i++) {
            if (number[i] > 0) {
                int temp = number[i];
                number[i] = number[0];
                number[0] = temp;
                break;
            }
        }
        for (int i = 0; i < numberCard; i++) {
            System.out.print(number[i]);
        }
    }
}